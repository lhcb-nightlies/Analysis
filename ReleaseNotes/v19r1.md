2017-10-18 Analysis v19r1
=========================

This version is released on the master branch.

It is based on Gaudi v29r0, LHCb v43r1, Lbcom v21r1, Rec v22r1 and Phys v24r1,
and uses LCG_91 with ROOT 6.10.06.

- Fix untested StatusCodes exposed by gaudi/Gaudi!386.
  - See merge request !190
- Ostap: minor fixes.
  - See merge request !189
- Fixed bug in number of unique IT channel IDs.
  - See merge request !176
- Remove overwriting of properties, requires to configure the tools separately.
  - See merge request !158
- Trivial fixes to McParticleFlow.
  - See merge request !165
- Update TupleToolTagging.cpp: I reverted to declaring the type of "DECISION".
  - See merge request !170
- Ostap: reshuffle a bit the code between Ostap  and AnalysisPython.
  - See merge request !168
- Ostap: fix TestFONLL test.
  - See merge request !167
- Ostap: fix all qmtests.
  - See merge request !162
- Ostap: add  trivial module stats.py with few helper statistical functions, clean-up.
 - See merge request !159
- Code to add Herschel FOM in tuple tool.
  - See merge request !157
- Ostap: minor adjustement for PidCalib machinery for Run-II.
  - See merge request !155
- Make Calo2MC tool public in BackgroundCategory and DaVinciSmartAssociator.
  - See merge request !156
- Fully qualify all enum value.
  - See merge request !146
- Ostap: improve PidCalib2 & pid_calib2: utilities to run PIDCalib for Run-II samples
  - See merge request !154
- Add the muon correlated chi2 to TupleToolPid.
  - See merge request !152
- Ostap: improve reweigting machinery, minor fixes.
  - See merge request !153
- Add new options to TupleToolDecayTreeFitter.
  - See merge request !142
- DaVinciMCTools : Fix daughters logic in PrintDecayTreeTool.
  - See merge request !149
- DecayTreeTuple - Fix TupleToolGeometry inconsistent branches.
  - See merge request !147
- Cleanup to TupleToolPi0Info.
  - See merge request !150
- Ostap: various fixes.
  - See merge request !145 and !151
- Do not run by default the Yandex MVAs.
  - See merge request !138
- Ostap: add more object methods, resolution models and operations
  - See merge request !137 and !144
