################################################################################
# Package: DaVinciMonitors
################################################################################
gaudi_subdir(DaVinciMonitors)

gaudi_depends_on_subdirs(Det/MuonDet
                         Event/HltEvent
                         Event/LinkerEvent
                         Event/RecEvent
                         Event/TrackEvent
                         Kernel/PartProp
                         Muon/MuonTools
                         Phys/DaVinciKernel
                         Phys/LoKiArrayFunctors
                         Phys/LoKiPhys)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DaVinciMonitors
                 src/*.cpp
                 LINK_LIBRARIES MuonDetLib HltEvent LinkerEvent RecEvent TrackEvent PartPropLib DaVinciKernelLib LoKiArrayFunctorsLib LoKiPhysLib)

gaudi_install_python_modules()

