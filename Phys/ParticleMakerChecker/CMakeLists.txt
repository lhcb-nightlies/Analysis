################################################################################
# Package: ParticleMakerChecker
################################################################################
gaudi_subdir(ParticleMakerChecker)

gaudi_depends_on_subdirs(Kernel/MCInterfaces
                         Phys/DaVinciKernel
                         Phys/DaVinciMCKernel)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(ParticleMakerChecker
                 src/*.cpp
                 INCLUDE_DIRS Kernel/MCInterfaces
                 LINK_LIBRARIES DaVinciKernelLib DaVinciMCKernelLib)

