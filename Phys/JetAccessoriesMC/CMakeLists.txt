################################################################################
# Package: JetAccessoriesMC
################################################################################
gaudi_subdir(JetAccessoriesMC)

gaudi_depends_on_subdirs(Kernel/LHCbMath
                         Phys/JetAccessories
                         Phys/JetTagging
                         Phys/LoKiAlgoMC
                         Phys/LoKiGenMC
                         Phys/LoKiPhysMC)

find_package(FastJet)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(JetAccessoriesMC
                 src/*.cpp
                 INCLUDE_DIRS FastJet
                 LINK_LIBRARIES FastJet LHCbMathLib LoKiAlgoMCLib LoKiGenMCLib LoKiPhysMCLib)

gaudi_install_headers(Kernel)
gaudi_install_python_modules()

