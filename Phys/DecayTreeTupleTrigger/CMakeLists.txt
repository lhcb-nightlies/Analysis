################################################################################
# Package: DecayTreeTupleTrigger
################################################################################
gaudi_subdir(DecayTreeTupleTrigger)

gaudi_depends_on_subdirs(Event/DAQEvent
                         Event/HltEvent
                         Event/L0Event
                         Kernel/HltInterfaces
                         Phys/DecayTreeTupleBase)

string(REPLACE "-pedantic" "" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(DecayTreeTupleTrigger
                 src/*.cpp
                 LINK_LIBRARIES DAQEventLib HltEvent L0Event HltInterfaces DecayTreeTupleBaseLib)

