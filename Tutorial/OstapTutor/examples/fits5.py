#!/usr/bin/env ostap 
# -*- coding: utf-8 -*-
# ============================================================================
#  @file
#  Example for simple fits with various background shapes
#  - simple polynomial
#  - monothonic increasing 
#  @author Vanya BELYAEV Ivan..Belyaev@itep.ru
#  @date   2014-12-10
# ============================================================================
"""Example for simple fits with various background shapes 
"""
# ============================================================================
## 1) import dataset and variable 
from   OstapTutor.TestData6 import *


import Ostap.FitModels as Models

## 2) create the model: signal

signal      = Models.Gauss_pdf ( 'Gauss' , mass = x , mean = 0.5 )


## just POSITIVE polynomial 
background = Models.PolyPos_pdf    ( 'B1' , mass = x , power = 3 )
##background = Models.Monothonic_pdf ( 'I1' , mass = x , power = 3 , increasing = True  ) ## data5, data2 
##background = Models.Monothonic_pdf ( 'D1' , mass = x , power = 3 , increasing = False ) ## data3, data4 

##background = Models.Convex_pdf     ( 'CTT1' , mass = x , power = 3 , increasing = True  , convex = True  ) ## data2 
##background = Models.Convex_pdf     ( 'CTF1' , mass = x , power = 3 , increasing = True  , convex = False ) ## data5
##background = Models.Convex_pdf     ( 'CFT1' , mass = x , power = 3 , increasing = False , convex = True  ) ## data3 
##background = Models.Convex_pdf     ( 'CFF1' , mass = x , power = 3 , increasing = False , convex = False ) ## data4 

##background = Models.ConvexOnly_pdf ( 'COT1' , mass = x , power = 3 , convex = True  ) ## data2,data3, (data1)
##background = Models.ConvexOnly_pdf ( 'COF1' , mass = x , power = 3 , convex = False ) ## data4,data5

background = Models.Sigmoid_pdf ( 'S1' , mass = x , power = 1 ) ## data6

model = Models.Fit1D (
    signal     = signal     ,
    background = background 
    )


r, f = model.fitTo ( data1 , silent = True , draw = True ) 



