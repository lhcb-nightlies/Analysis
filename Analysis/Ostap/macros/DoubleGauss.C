// ============================================================================
// STD& STL
// ============================================================================
#include <cmath>
// ============================================================================
// ROOT 
// ============================================================================
#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooRealProxy.h"
// ============================================================================
namespace Analysis
{
  // ==========================================================================
  namespace Models 
  {
    // ========================================================================
    /** @class  DoubleGauss
     *  Simple double Gaussian PDF
     *  suitable as resolution model
     */
    class DoubleGauss: public RooAbsPdf 
    {
    public:
      // ======================================================================
      ClassDef(Analysis::Models::DoubleGauss, 1) ;
      // ======================================================================
    public:
      // ======================================================================
      /// constructor from all parameters
      DoubleGauss ( const char*          name      , 
                    const char*          title     ,
                    RooAbsReal&          x         ,
                    RooAbsReal&          sigma     ,   // narrow sigma 
                    RooAbsReal&          fraction  ,   // fraction of narrow sigma 
                    RooAbsReal&          scale     ,   // wide/narrow sigma ratio    
                    RooAbsReal&          mean      ) ; // mean, presumably fixed at 0
      /// "copy" constructor 
      DoubleGauss ( const DoubleGauss& , const char* name = 0 ) ;
      /// virtual destructor 
      virtual ~DoubleGauss(){} ;
      /// clone 
      virtual  DoubleGauss* clone ( const char* name ) const ; 
      // =====================================================================
    public: // some fake functionality
      // =====================================================================
      // fake default contructor, needed just for proper (de)serialization 
      DoubleGauss () ;
      // =====================================================================
    public:
      // =====================================================================
      Int_t    getAnalyticalIntegral 
      ( RooArgSet&  allVars       , 
        RooArgSet&  analVars      , 
        const char* rangeName = 0 ) const ;
      Double_t analyticalIntegral
      ( Int_t       code          , 
        const char* rangeName = 0 ) const ;
      // =====================================================================
    public:
      // =====================================================================
      // the actual evaluation of function 
      virtual Double_t evaluate() const ;
      // =====================================================================
    protected:
      // =====================================================================
      RooRealProxy m_x         ;
      RooRealProxy m_sigma     ;
      RooRealProxy m_fraction  ;
      RooRealProxy m_scale     ;
      RooRealProxy m_mean      ;
      // ======================================================================
    } ; 
    // ========================================================================
  } 
  // ==========================================================================
}
// ============================================================================
ClassImp(Analysis::Models::DoubleGauss) ;
// ============================================================================
// constructor from all parameters 
// ============================================================================
Analysis::Models::DoubleGauss::DoubleGauss
( const char*          name      , 
  const char*          title     ,
  RooAbsReal&          x         ,
  RooAbsReal&          sigma     ,  // narrow sigma 
  RooAbsReal&          fraction  ,  // fraction of narrow sigma 
  RooAbsReal&          scale     ,  // wide/narrow sigma ratio    
  RooAbsReal&          mean      )  // mean, presumably  fixed at 0
  //
  : RooAbsPdf( name , title ) 
  , m_x        ( "x"         , "Observable"          , this , x        ) 
  , m_sigma    ( "sigma"     , "Narrow sigma"        , this , sigma    ) 
  , m_fraction ( "fraction"  , "Fraction"            , this , fraction ) 
  , m_scale    ( "scale"     , "Scale"               , this , scale    ) 
  , m_mean     ( "mean"      , "Mean"                , this , mean     ) 
{}
// ============================================================================
// "copy" constructor 
// ============================================================================
Analysis::Models::DoubleGauss::DoubleGauss
( const Analysis::Models::DoubleGauss& right ,  
  const char*                          name  ) 
  : RooAbsPdf ( right , name ) 
    //
  , m_x        ( "x"        , this , right.m_x        ) 
  , m_sigma    ( "sigma"    , this , right.m_sigma    )
  , m_fraction ( "fraction" , this , right.m_fraction )
  , m_scale    ( "scale"    , this , right.m_scale    )
  , m_mean     ( "mean"     , this , right.m_mean    )
{}
// ============================================================================
// clone it!
// ============================================================================
Analysis::Models::DoubleGauss*
Analysis::Models::DoubleGauss::clone( const char* name ) const 
{ return new Analysis::Models::DoubleGauss(*this) ; }
// ============================================================================
// the actual evaluation of function 
// ============================================================================
Double_t Analysis::Models::DoubleGauss::evaluate() const 
{
  //
  const double x        = m_x        ;
  const double mu       = m_mean     ;
  const double sigma    = m_sigma    ;
  const double scale    = m_scale    ;
  const double fraction = m_fraction ;
  //
  const double sigma2   =  scale *  sigma ;
  //
  const double dx1      = ( x - mu ) / sigma  ;
  const double dx2      = ( x - mu ) / sigma2 ;
  //
  const double  f1 =  std::max ( 0.0 , std::min ( fraction , 1.0 ) ) ;
  const double  f2 =  1 - f1 ;
  //
  static const double s_norm = 1.0 / std::sqrt ( 2.0 * M_PI  ) ;
  //
  return 
    s_norm * ( f1 * std::exp ( -0.5 * dx1 * dx1 ) / sigma  +
               f2 * std::exp ( -0.5 * dx2 * dx2 ) / sigma2 ) ;
}
// ============================================================================
Int_t Analysis::Models::DoubleGauss::getAnalyticalIntegral
( RooArgSet&  allVars       , 
  RooArgSet&  analVars      ,
  const char* /*rangeName*/ ) const
{
  if (matchArgs ( allVars , analVars , m_x ) ) { return 1 ; }
  return 0 ;
}

// ============================================================================
Double_t Analysis::Models::DoubleGauss::analyticalIntegral
( Int_t       code      , 
  const char* rangeName ) const
{
  assert( code == 1 ) ;
  
  const double x        = m_x        ;
  const double mu       = m_mean     ;
  const double sigma    = m_sigma    ;
  const double scale    = m_scale    ;
  const double fraction = m_fraction ;
  //
  const double sigma2   =  scale *  sigma ;
  //
  const double dx1      = ( x - mu ) / sigma  ;
  const double dx2      = ( x - mu ) / sigma2 ;
  //
  const double  f1 =  std::max ( 0.0 , std::min ( fraction , 1.0 ) ) ;
  const double  f2 =  1 - f1 ;
  //
  const double  xmax = m_x.max ( rangeName )  ;
  const double  xmin = m_x.min ( rangeName )  ;
  //
  static const double s_isqrt2 = 1.0 / std::sqrt ( 2.0 ) ;
  //
  const double ixscale1 = s_isqrt2 / sigma  ;
  const double ixscale2 = s_isqrt2 / sigma2 ;
  //
  const double r1 = 
    std::erf ( ( xmax - mu ) * ixscale1 ) - 
    std::erf ( ( xmin - mu ) * ixscale1 ) ;
  //
  const double r2 = 
    std::erf ( ( xmax - mu ) * ixscale2 ) - 
    std::erf ( ( xmin - mu ) * ixscale2 ) ;
  //
  return 0.5 * ( f1 * r1 + f2 * r2  ) ;  
}

// ============================================================================
// The END 
// ============================================================================


