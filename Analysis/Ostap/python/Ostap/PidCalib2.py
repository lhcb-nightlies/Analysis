#!/usr/bin/env python
# -*- coding: utf-8 -*-
# =============================================================================
## @file  Ostap/PidCalib2.py
#  Module to run PIDCalib machinery for RUN-II data
#  @see https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/ChargedPID
# =============================================================================
""" Module to run PIDCalib machinery for RUN-II data
- see https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/ChargedPID
"""
# =============================================================================
__version__ = "$Revision$"
__author__  = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__    = "2011-06-07"
__all__     = (
    'run_pid_calib' , ## run pid-calib machinery
    'PARTICLE_2D'   , ## helper  object to produce 2D efficiency histogram 
    'PARTICLE_3D'   , ## helper  object to produce 3D efficiency histogram 
    )
# =============================================================================
import ROOT,os
from   Ostap.Logger import getLogger, setLogging 
if '__main__' == __name__ : logger = getLogger ( 'Ostap.PidCalib2' )
else                      : logger = getLogger ( __name__          )
import Ostap.ZipShelve as DBASE 

# =============================================================================
## PIDCALIB data samples
#  https://twiki.cern.ch/twiki/bin/view/LHCbPhysics/ChargedPID
samples  = {
    ## PIDCalibTuples v4r1     
    'v4r1' : {
    'pp/2015/MagUp'   : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00057800/0000/' ,
    'pp/2015/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00057802/0000/' ,
    'pp/2016/MagUp'   : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00056408/0000/' ,
    'pp/2016/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00056409/0000/' ,
    'pA/2016/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Protonion16/PIDCALIB.ROOT/00058286/0000/' ,
    'Ap/2016/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Ionproton16/PIDCALIB.ROOT/00058288/0000/' ,
    },
    ## PIDCalibTuples v5r1 
    'v5r1' : {
    'pp/2015/MagUp'   : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00064787/0000/' ,
    'pp/2015/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision15/PIDCALIB.ROOT/00064785/0000/' ,
    'pp/2016/MagUp'   : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064793/0000/' ,
    'pp/2016/MagDown' : '/eos/lhcb/grid/prod/lhcb/LHCb/Collision16/PIDCALIB.ROOT/00064795/0000/' ,
    }
    }
PARTICLES = {
    'v4r1' : {
    2015 : {
    'ELECTRONS' : ['B_Jpsi_EP'  , 'Jpsi_EP'] ,
    'KAONS'     : ['DSt3Pi_KP'  , 'DSt_KP'   , 'DsPhi_KP'   , 'Phi_KP'  ] ,
    'MUONS'     : ['B_Jpsi_MuP' , 'DsPhi_MuP', 'Jpsi_MuP'   , 'Phi_MuP' ] ,
    'PIONS'     : ['DSt3Pi_PiP' , 'DSt_PiP'  , 'KS_PiP'     ]             ,
    'PROTONS'   : ['Lam0_HPT_P' , 'Lam0_P'   ,                           ## 'B_Jpsi_P'   , 'Jpsi_P' 
                   'Lam0_VHPT_P', 'LbLcMu_P' , 'LbLcPi_P'   , 'Sigmac0_P' , 'Sigmacpp_P' ]
    },
    2016 : {
    'KAONS'     : ['DSt_KP'     , 'DsPhi_KP'  , 'Ds_KP'    ]  ,
    'MUONS'     : ['B_Jpsi_MuP' , 'DsPhi_MuP' , 'Jpsi_MuP' ]  ,
    'PIONS'     : ['DSt_PiP'    , 'KS_PiP'    ]               ,
    'PROTONS'   : ['Lam0_HPT_P' , 'Lam0_P'    , 'Lam0_VHPT_P' , 'LbLcMu_P' ] 
    }} ,
    'v5r1' : {
    2015: {
    'ELECTRONS' : ['B_Jpsi_EP']  ,
    'KAONS'     : ['DSt_KP'      , 'DsPhi_KP'  ]                  ,
    'MUONS'     : ['B_Jpsi_MuP'  , 'Jpsi_MuP'  ] ,
    'PIONS'     : ['DSt_PiP'     , 'KSLL_PiP'  ]                  ,
    'PROTONS'   : ['Lam0LL_HPT_P', 'Lam0LL_P'  , 'Lam0LL_VHPT_P'  , 'LbLcMu_P' ]
    } ,
    2016 : {
    'ELECTRONS' : ['B_Jpsi_EP'   ]   ,
    'KAONS'     : ['DSt_KP'      , 'DsPhi_KP' ] ,
    'MUONS'     : ['B_Jpsi_MuP'  , 'DsPhi_MuP', 'Jpsi_MuP'      ] ,
    'PIONS'     : ['DSt_PiP'     , 'KSLL_PiP' ] ,
    'PROTONS'   : ['Lam0LL_HPT_P', 'Lam0LL_P' , 'Lam0LL_VHPT_P' , 'LbLcMu_P'     ] 
    }}}
# =============================================================================
# 2015 v4r1 
# Ostap.PidCalib2           INFO    Found sample ELECTRONS : ['B_Jpsi_EP', 'Jpsi_EP']
# Ostap.PidCalib2           INFO    Found sample     KAONS : ['DSt3Pi_KP', 'DSt_KP', 'DsPhi_KP', 'Phi_KP']
# Ostap.PidCalib2           INFO    Found sample     MUONS : ['B_Jpsi_MuP', 'DsPhi_MuP', 'Jpsi_MuP', 'Phi_MuP']
# Ostap.PidCalib2           INFO    Found sample     OTHER : ['DsPhi_KM_notag', 'DsPhi_KP_notag', 'Lam0_P_isMuon', 'Lam0_Pbar_isMuon', 'Phi_KM_notag', 'Phi_KP_notag']
# Ostap.PidCalib2           INFO    Found sample     PIONS : ['DSt3Pi_PiP', 'DSt_PiP', 'KS_PiP']
# Ostap.PidCalib2           INFO    Found sample   PROTONS : ['B_Jpsi_P', 'Jpsi_P', 'Lam0_HPT_P', 'Lam0_P', 'Lam0_VHPT_P', 'LbLcMu_P', 'LbLcPi_P', 'Sigmac0_P', 'Sigmacpp_P']

# 2016 v4r1 
# Ostap.PidCalib2           INFO    Found sample     KAONS : ['DSt_KP', 'DsPhi_KP', 'Ds_KP']
# Ostap.PidCalib2           INFO    Found sample     MUONS : ['B_Jpsi_MuP', 'DsPhi_MuP', 'Jpsi_MuP']
# Ostap.PidCalib2           INFO    Found sample     OTHER : ['Lam0_P_isMuon', 'Lam0_Pbar_isMuon']
# Ostap.PidCalib2           INFO    Found sample     PIONS : ['DSt_PiP', 'KS_PiP']
# Ostap.PidCalib2           INFO    Found sample   PROTONS : ['Lam0_HPT_P', 'Lam0_P', 'Lam0_VHPT_P', 'LbLcMu_P']

# 2015 v5r1 
# Ostap.PidCalib2           INFO    Found sample ELECTRONS : ['B_Jpsi_EP']
# Ostap.PidCalib2           INFO    Found sample     KAONS : ['DSt_KP', 'DsPhi_KP']
# Ostap.PidCalib2           INFO    Found sample     MUONS : ['B_Jpsi_MuP', 'Jpsi_MuP', 'Jpsinopt_MuP']
# Ostap.PidCalib2           INFO    Found sample     OTHER : ['Lam0LL_P_isMuon', 'Lam0LL_Pbar_isMuon']
# Ostap.PidCalib2           INFO    Found sample     PIONS : ['DSt_PiP', 'KSLL_PiP']
# Ostap.PidCalib2           INFO    Found sample   PROTONS : ['Lam0LL_HPT_P', 'Lam0LL_P', 'Lam0LL_VHPT_P', 'LbLcMu_P']

# 2016 v5r1 
# Ostap.PidCalib2           INFO    Found sample ELECTRONS : ['B_Jpsi_EP']
# Ostap.PidCalib2           INFO    Found sample     KAONS : ['DSt_KP', 'DsPhi_KP']
# Ostap.PidCalib2           INFO    Found sample     MUONS : ['B_Jpsi_MuP', 'DsPhi_MuP', 'Jpsi_MuP', 'Jpsinopt_MuP']
# Ostap.PidCalib2           INFO    Found sample     OTHER : ['Lam0LL_P_isMuon', 'Lam0LL_Pbar_isMuon']
# Ostap.PidCalib2           INFO    Found sample     PIONS : ['DSt_PiP', 'KSLL_PiP']
# Ostap.PidCalib2           INFO    Found sample   PROTONS : ['Lam0LL_HPT_P', 'Lam0LL_P', 'Lam0LL_VHPT_P', 'LbLcMu_P']
# =============================================================================
## TTree names
electrons = set()
kaons     = set()
muons     = set()
pions     = set()
protons   = set()
for v in PARTICLES :
    for y in PARTICLES[v] :
        electrons |= set( PARTICLES[v][y].get ( 'ELECTRONS', [] ) )
        kaons     |= set( PARTICLES[v][y].get ( 'KAONS'    , [] ) )
        muons     |= set( PARTICLES[v][y].get ( 'MUONS'    , [] ) )
        pions     |= set( PARTICLES[v][y].get ( 'PIONS'    , [] ) )
        protons   |= set( PARTICLES[v][y].get ( 'PROTONS'  , [] ) )

e_plus       = tuple ( electrons )
e_minus      = tuple ( e.replace('_EP' ,'_EM' ) for e in     e_plus ) 
kaons_plus   = tuple ( kaons   )
kaons_minus  = tuple ( k.replace('_KP' ,'_KM' ) for k in kaons_plus ) 
muons_plus   = tuple ( muons )
muons_minus  = tuple ( m.replace('_MuP','_MuM') for m in muons_plus ) 
pions_plus   = tuple ( pions )
pions_minus  = tuple ( p.replace('_PiP','_PiM') for p in pions_plus ) 
protons      = tuple ( protons ) 
antiprotons  = tuple ( p + 'bar' for p in protons )

for v in PARTICLES :
    for y in PARTICLES[v] :
        for k in  PARTICLES[v][y] :
            ps = set ( PARTICLES[v][y][k] ) 
            if   'ELECTRONS' ==   k : ps |= set ( p.replace('_EP'  ,'_EM'   ) for p in ps )
            elif 'KAONS'     ==   k : ps |= set ( p.replace('_KP'  ,'_KM'   ) for p in ps )
            elif 'MUONS'     ==   k : ps |= set ( p.replace('_MuP' ,'_MuM'  ) for p in ps )
            elif 'PIONS'     ==   k : ps |= set ( p.replace('_PiP' ,'_PiM'  ) for p in ps )
            elif 'PROTONS'   ==   k : ps |= set ( p.replace('_P'   ,'_Pbar' ) for p in ps )
            PARTICLES[v][y][k]  =    tuple ( ps )

# =============================================================================
## prepare the parser 
def make_parser () :
    """ Prepare the parser
    - oversimplified version of parser from MakePerfHistsRunRange.py script 
    """
    import argparse, os, sys 
    
    parser = argparse.ArgumentParser (
        formatter_class = argparse.RawDescriptionHelpFormatter,
        prog            = os.path.basename(sys.argv[0]),
        description     = """Make performance histograms for a given:
        a) data taking period <YEAR>        ( e.g. 2015    )
        b) magnet polarity    <MAGNET>      ( 'MagUp' or 'MagDown' or 'Both' )
        c) particle type      <PARTICLE>    ( 'K', 'P' , 'Pi' , 'e' , 'Mu'   )  
        """ , 
        epilog =
        """TO BE UPDATED!!!
        To use the 'MuonUnBiased' hadron samples for muon misID studies, one of the
        following tracks types should be used instead: \"K_MuonUnBiased\", \"Pi_MuonUnBiased\"
        or \"P_MuonUnBiased\"."""
        )
    
    ## add mandatory arguments
    parser.add_argument (
        'particle'    ,
        metavar = '<PARTICLE>'    , type=str    ,
        choices = \
        ( 'p'  , 'p+'  , 'p-'  ) + ( 'P'  , 'P+'  , 'P-'  ) + protons    + antiprotons + \
        ( 'K'  , 'K+'  , 'K-'  ) + ( 'k'  , 'k+'  , 'k-'  ) + kaons_plus + kaons_minus + \
        ( 'pi' , 'pi+' , 'pi-' ) + ( 'Pi' , 'Pi+' , 'Pi-' ) +
        ( 'PI' , 'PI+' , 'PI-' ) + pions_plus + pions_minus + \
        ( 'e'  , 'e+'  , 'e-'  ) + ( 'E'  , 'E+'  , 'E-'  ) + e_plus     + e_minus     + \
        ( 'mu' , 'mu+' , 'mu-' ) + ( 'Mu' , 'Mu+' , 'Mu-' ) + 
        ( 'MU' , 'MU+' , 'MU-' ) + muons_plus + muons_minus , 
        help    = "Sets the particle type"     )

    parser.add_argument ( '-y' , '--year'            , 
                          metavar = '<YEAR>'         , type=int ,
                          choices = ( 2015 , 2016 )  ,                           
                          help    = "Data taking period"  )
    
    parser.add_argument ( '-x'  , '--collisions'      , default = 'pp' ,
                          metavar = '<COLLISIONS>'    , type=str       ,
                          choices = ( 'pp' , 'pA' , 'Ap' )  ,                           
                          help    = "Collision type"  )
   
    ## add the optional arguments
    parser.add_argument ( '-p' , '--polarity'             , default = 'Both' , 
                          metavar = '<MAGNET>'            , type=str ,
                          choices = ( 'MagUp' , 'MagDown' , 'Both' ) , 
                          help    = "Sets the magnet polarity"     )
    
    parser.add_argument ( '-f', '--maxfiles     ',
                          dest    = "MaxFiles"   ,
                          metavar = "<NUM>"      , type=int, default=-1 , 
                          help    = "The maximum number of calibration files to process")
    
    parser.add_argument ( '-c', '--cuts', dest='cuts', metavar='<CUTS>', default='',
                          help="""List of cuts to apply to the calibration sample
                          prior to determine the PID efficiencies, 
                          e.g. fiducial volume,  HASRICH, etc... 
                          """)
    parser.add_argument ( '-o' , '--output'       ,
                          type    = str           ,
                          default = 'PIDCALIB.db' ,
                          help = "The name of output database file" )
    
    parser.add_argument ( '-v' , '--version'             , default = 'v5r1' , 
                          metavar = '<VERSION>'          , type=str ,
                          choices = ( 'v4r1', 'v5r1' ) , 
                          help    = "Version of PIDCalibTuples to be used")
    
    parser.add_argument ( '-t' , '--testfiles'    , default = '' , 
                          dest    = 'TestPattern' ,
                          metavar = '<TESTFILES>' , type =  str ,
                          help    = 'The pattern to load test-files (year, polarity, etc are ignored)' )
    
    parser.add_argument ( '-s' , '--samples'      , default = [], 
                          dest    = 'Samples'     ,
                          metavar = '<SAMPLES>'   , nargs = '*' ,
                          help    = 'The samples to be processed' )
    
    addGroup = parser.add_argument_group("further options")
    addGroup.add_argument ("-q", "--quiet", dest="verbose", action="store_false",
                           default=True,
                           help="Suppresses the printing of verbose information")
    
    return parser

# =============================================================================
## load certain calibration files  using given file patterns
def load_data ( pattern          ,
                particles        ,
                tag      = ''    , 
                maxfiles = -1    ,
                verbose  = False , 
                data     = {}    ) :
    """Load certain calibration files  using given file patterns
    """
    
    from Ostap.Data import Data as Data
    for p in particles :
        chain  = p + 'Tuple/DecayTree'
        d               = Data ( chain , pattern , maxfiles = maxfiles , silent = not verbose )
        key = '%s/%s' % ( tag , p )
        
        logger.info ( 'Loaded data for key %s: %s'   % ( key , d ) ) 
        if not d :
            logger.warning('No useful data is found for %s' % key )
            continue
        
        data [ key ] = d
        
        if verbose :
            import Ostap.TreeDeco 
            logger.info ('Loaded %s branches %s' % ( d , d.chain.branches() ) )
            import Ostap.TFileDeco 
            f  = d.files[0]
            ff = ROOT.TFile.Open ( f )
            ff.ls   ()
            keys     = ff.keys()
            ss       = set()
            for k in keys :
                p = k.find('/DecayTree')
                if 0 < p : ss.add (  k[:p] )
            from collections import defaultdict 
            sk = defaultdict(list) 
            for s in ss :
                s     = s.replace('Tuple','')
                a,b,q = s.rpartition('_')
                if       q in ( 'PiP' , )  : sk ['PIONS'    ] . append ( s )
                elif     q in ( 'MuP' , )  : sk ['MUONS'    ] . append ( s )
                elif     q in ( 'KP'  , )  : sk ['KAONS'    ] . append ( s )
                elif     q in ( 'P'   , )  : sk ['PROTONS'  ] . append ( s )
                elif     q in ( 'EP'  , )  : sk ['ELECTRONS'] . append ( s )
                elif not q in ( 'PiM' , 'MuM' , 'KM' , 'Pbar' , 'EM' ) :
                    sk['OTHER'].append ( s )
                    
            keys = sk.keys()
            keys.sort()
            for k in keys :
                sk[k].sort() 
                logger.info('Found sample %9s : %s' % (  k , sk[k] ) )
                
            ff.Close()                            

    return data
                    

# =============================================================================
## Load calibration samples 
def load_samples ( particles                                 ,
                   years      = ( '2015'    , '2016'       ) ,
                   collisions = ( 'pp'      , 'pA'  , 'Ap' ) ,
                   polarity   = ( 'MagDown' , 'MagUp'      ) ,
                   version    = 'v5r1'                       ,
                   maxfiles   = -1                           ,
                   verbose    = False                        ) :
    """Load calibration samples
    """
     
    
    if  0 < maxfiles : logger.warning('Only max=%d files will be processed!' % maxfiles )
    
    maxfiles = maxfiles if 0 < maxfiles  else 1000000
    data     = {}
    for y in years :
        for c in collisions : 
            for p in polarity :
                tag  = '%s/%s/%s' % ( c , y , p )
                fdir = samples[version].get ( tag , None )
                if not fdir :
                    logger.warning('No data is found for Collisions="%s" , Year="%s" , Polarity="%s"' % ( c , y , p ) )
                    continue

                ## file pattern:
                pattern = fdir + '*.pidcalib.root'

                ## load files 
                data = load_data ( pattern , particles , tag , maxfiles , verbose , data ) 

    return data


# =============================================================================
## Run PID-calib machinery 
def run_pid_calib ( FUNC , args = [] ) :
    """ Run PID-calib procedure 
    """
    
    import sys
    vargs   = args + [ a for a in sys.argv[1:] if '--' != a ]
    
    parser  = make_parser        ()
    config  = parser.parse_args ( vargs )
    
    if config.verbose :  
        import Ostap.Line 
        logger.info ( __file__  + '\n' + Ostap.Line.line  ) 
        logger.info ( 80*'*'   )
        logger.info ( __doc__  )
        logger.info ( 80*'*'   )
        _vars   = vars ( config )
        _keys   = _vars.keys()
        _keys .sort()
        logger.info ( 'PIDCalib configuration:')
        for _k in _keys : logger.info ( '  %15s : %-s ' % ( _k , _vars[_k] ) )
        logger.info ( 80*'*'   )
        setLogging(2) 
        
    if config.verbose : from Ostap.Logger import logInfo    as useLog
    else              : from Ostap.Logger import logWarning as useLog 
        
    from Ostap.Logger import logInfo    as useLog
    with useLog() :
        return pid_calib  ( FUNC , config ) 


def  treat_arguments ( config )  :
    pass 

    
# =============================================================================
## Run PID-calib machinery 
def pid_calib ( FUNC , config  ) : 
    """ Run PID-calib procedure 
    """

    particle  = config.particle
    particles   = [ particle ]
    if   'P'   == particle.upper()  : particles = protons    + antiprotons
    elif 'P+'  == particle.upper()  : particles = protons 
    elif 'P-'  == particle.upper()  : particles = antiprotons 
    elif 'PI'  == particle.upper()  : particles = pions_plus + pions_minus 
    elif 'PI+' == particle.upper()  : particles = pions_plus 
    elif 'PI-' == particle.upper()  : particles = pions_minus  
    elif 'K'   == particle.upper()  : particles = kaons_plus + kaons_minus 
    elif 'K+'  == particle.upper()  : particles = kaons_plus 
    elif 'K-'  == particle.upper()  : particles = kaons_minus  
    elif 'E'   == particle.upper()  : particles = e_plus + e_minus 
    elif 'E+'  == particle.upper()  : particles = e_plus 
    elif 'E-'  == particle.upper()  : particles = e_minus  
    elif 'MU'  == particle.upper()  : particles = muons_plus + muons_minus 
    elif 'MU+' == particle.upper()  : particles = muons_plus 
    elif 'MU-' == particle.upper()  : particles = muons_minus  

    polarity  =  config.polarity
    
    if 'Both' == polarity  : polarity  = [ 'MagUp'  , 'MagDown' ]
    else                   : polarity  = [ polarity ]

    year      =  config.year 
    
    pl    = PARTICLES[config.version][year]
    known =  set() 
    for p in pl : known |= set( pl[p] )

    particles = set   ( particles ) & known 
    particles = tuple ( particles )

    if config.collisions in ( 'pA' , 'Ap' ) :
        if 2016 != year :
            logger.error('There are no %s samples for % year' % ( config.collisions , year ) )
            return
        if 'MagUp' in polarity :
            polarity.remove ( 'MagUp' )
            logger.warning  ( 'Only MagDown samples are available for %s/%s' % ( config.collisions , year ) )
        if 'v4r1' != config.version :
            logger.warning ("Only PIDCalibTuples v4r1 samples exist for %s/%s"    % ( config.collision  , year ) )
            config.version = 'v4r1'

    ## remove some buggy stuff 
    to_remove = set()

    if   'v4r1' == config.version and 2015 == year :
        to_remove |= set( [ 'Sigmac0_P'  , 'Sigmac0_Pbar'  ,   ## buggy samples: sWeight==1
                            'Sigmacpp_P' , 'Sigmacpp_Pbar' ,   ## ditto
                            'LbLcMu_P'   , 'LbLcMu_Pbar'   ,   ## ditto 
                            'LbLcPi_P'   , 'LbLcPi_Pbar'   ,   ## ditto
                            'DSt3Pi_KP'  , 'DSt3Pi_KM'     ,   ## very low eff: buggy?
                            'Phi_KP'     , 'Phi_KM'        ,   ## ditto
                            ])
    elif 'v4r1' == config.version and 2016 == year :
        to_remove |= set( [ 'LbLcMu_P'   , 'LbLcMu_Pbar'   ,   ## ditto
                            'Ds_KP'      , 'Ds_KM'         ] ) ## ditto

    ## remove samples
    particles = tuple ( set ( particles ) - to_remove )
    
    if config.Samples :
        particles  = tuple ( config.Samples )

    year       = [ year ]
    collisions = [ config.collisions ] 
    logger.info ( 'Data taking periods : %s' % year               )  
    logger.info ( 'Collisions          : %s' % collisions         )  
    logger.info ( 'Magnet polarities   : %s' % polarity           )  
    logger.info ( 'Particles           : %s' % list ( particles ) )  
    logger.info ( 80*'*'   )
     
    ##import Ostap.Kisa

    #
    ## Load PID samples
    #

    if not config.TestPattern: 
        data = load_samples ( particles                    ,
                              years      = year            ,
                              collisions = collisions      ,
                              polarity   = polarity        ,
                              version    = config.version  , 
                              verbose    = config.verbose  ,
                              maxfiles   = config.MaxFiles )
    else :
        data = load_data ( config.TestPattern         ,
                           particles                  ,
                           tag      = 'Test'          ,
                           maxfiles = config.MaxFiles ,
                           verbose  = config.verbose  )
    #
    ## Start processing
    #
    
    fun       = FUNC ()
    tacc      = None 
    trej      = None
    files     = set()
    
    keys = data.keys()
    keys.sort()
    processed = set() 
    for k in keys :

        d         = data[k]
        logger.info ( 'Processing  %s' % k )

        ## small chain 
        sm = d.chain[:1]
        c1 = sm.statVar('probe_sWeight'        )
        c2 = sm.statVar('probe_Brunel_sWeight' )
        if 0 == c1.rms() :
            v1 = c1.values()
            logger.error("``probe_sWeight''        variable is trivial: mean/(min,max)=%s/(%s,%s)" % ( c1.mean() , v1.min() , v1.max() ) ) 
        if 0 == c2.rms() :
            v2 = c2.values()
            logger.error("``probe_Brunel_sWeight'' variable is trivial: mean/(min,max)=%s/(%s,%s)" % ( c2.mean() , v2.min() , v2.max() ) ) 
            
        acc , rej = fun.run ( d.chain )
        na   = acc.sum_bins()/1000
        nr   = rej.sum_bins()/1000
        
        heff = 100./( 1. +  rej/acc )
        hst  = heff.stat()
        del heff

        logger.info ( "%s: Accepted/rejected/efficiency/(min,max): %sk/%sk/(%.1f+-%.1f)%%/(%.1f,%.1f)%%" % ( k ,
                                                                                                             na.toString('(%.1f+-%.1f)') ,
                                                                                                             nr.toString('(%.1f+-%.1f)') ,
                                                                                                             hst.mean() , hst.rms()  ,
                                                                                                             hst.min () , hst.max() ) )
        if tacc     : tacc += acc
        else        : tacc  = acc.clone()
        
        if trej     : trej += rej
        else        : trej  = rej.clone()

        for f in d.files : files.add ( f )

        processed.add ( k ) 
        import datetime 
        now =   datetime.datetime.now() 
        with DBASE.open( config.output ) as db :
            db [ k                                      ] =  acc ,  rej 
            db [ k + ':data'                            ] = d
            db [ k + ':created'                         ] = now 
            db [ k + ':conf'                            ] = config 
            db [ 'TOTAL_%s'           % config.particle ] = tacc , trej
            db [ 'TOTAL_%s:keys'      % config.particle ] = keys  
            db [ 'TOTAL_%s:files'     % config.particle ] = files  
            db [ 'TOTAL_%s:conf'      % config.particle ] = config 
            db [ 'TOTAL_%s:created'   % config.particle ] = now 
            db [ 'TOTAL_%s:processed' % config.particle ] = processed 

    if os.path.exists ( config.output ) : 
        with DBASE.open( config.output , 'r') as db :
            logger.info('Output DBASE with results: %s' % config.output )
            db.ls()
            try : 
                ta,tr = db [ 'TOTAL_%s'       % config.particle ]
                
                na    = ta.sum_bins()/1000
                nr    = tr.sum_bins()/1000
                
                heff  = 100./( 1. + tr/ta )
                hst   = heff.stat()
                del heff
                
                logger.info ( "%s: ACCEPTED/REJECTED/efficiency/(min,max): %sk/%sk/(%.1f+-%.1f)%%/(%.1f,%.1f)%%" % ( k ,
                                                                                                                     na.toString('(%.1f+-%.1f)') ,
                                                                                                                     nr.toString('(%.1f+-%.1f)') ,
                                                                                                                     hst.mean() , hst.rms()  ,
                                                                                                                     hst.min () , hst.max() ) )
            except :
                pass 

    logger.info ( 'Processed: Data taking periods : %s' % year               )  
    logger.info ( 'Processed: Collisions          : %s' % collisions         )  
    logger.info ( 'Processed: Magnet polarities   : %s' % polarity           )  
    logger.info ( 'Processed: Particles           : %s' % list ( particles ) )  
    logger.info ( 'Processed: Keys                : %s' % keys               )  

            
    return data

# =============================================================================
## the actual function to fill PIDcalib histograms 
#  - it books two histograms  (2D in this case)
#  - it fill them with 'accepted' and 'rejected' events (2D in this case)
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-05-10
class PARTICLE_2D(object) :
    """The actual function to fill PIDcalib histograms 
    - it books two histograms  (2D in this case)  ``accepted'' and ``rejected''
    - it fill them with 'accepted' and 'rejected' events (2D in this case)
    - finally one calculates the efficiency as
    >>> efficiency = 1/( 1 + rejected/accepted)
    """
    ## create the object 
    def __init__ ( self             ,
                   accepted         , ## accepted sample 
                   rejected         , ## rejected sample 
                   xbins            , ## bins in 1st axis
                   ybins            , ## bins in 2nd axis 
                   cuts     = None  , ## additional cuts (if any) 
                   ## "Accept"-function                              what  to project/draw                                 cuts&weight 
                   acc_fun  = lambda s,data : data.pproject ( s.ha , 'probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.accepted , silent = True ) ,
                   ## "Reject"-function                              what  to project/draw                                 cuts&weight 
                   rej_fun  = lambda s,data : data.pproject ( s.hr , 'probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.rejected , silent = True ) ) :
        #
        ## the heart of the whole game:   DEFINE PID CUTS! 
        # 
        self.accepted  = accepted 
        self.rejected  = rejected 
        
        logger.info ( "ACCEPTED: %s" % self.accepted ) 
        logger.info ( "REJECTED: %s" % self.rejected ) 
        
        # 
        ## book 2D-histograms
        #
        import ROOT
        from Ostap.PyRoUts import h2_axes
        
        self.ha  = h2_axes ( xbins , ybins , title = 'Accepted(%s)' % self.accepted ) 
        self.hr  = h2_axes ( xbins , ybins , title = 'Rejected(%s)' % self.rejected )

        self.cuts = cuts
        
        if self.cuts : ## redefine accepted/rejected 
            self.accepted = '(%s)*(%s)' % ( self.cuts , self.accepted )
            self.rejected = '(%s)*(%s)' % ( self.cuts , self.rejected )

        self.acc_fun = acc_fun
        self.rej_fun = rej_fun

    ## the  function :-) 
    def __call__ ( self , data ) : return self.run ( data )
    
    ## The actual function to fill PIDCalib histograms
    def run ( self , data ) :        
        """The actual function to fill PIDCalib histograms
        - it fills histograms with 'accepted' and 'rejected' events (2D in this case)
        - ``data'' is a tree/chain from PIDCalib
        """

        #
        ## we need here ROOT and Ostap machinery!
        #
        na , ha = self.acc_fun ( self , data ) 
        nr , hr = self.rej_fun ( self , data ) 

        #
        ## prepare the output
        #

        logger.debug ( 'Accepted/rejected entries: %ld/%ld' % ( na , nr ) )
        
        ha.SetName   ( "Accepted" )
        hr.SetName   ( "Rejected" )        

        return ha , hr 


# =============================================================================
## the actual function to fill PIDcalib histograms 
#  - it books two histograms  (3D in this case)
#  - it fill them with 'accepted' and 'rejected' events (3D in this case)
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2014-05-10
class PARTICLE_3D(PARTICLE_2D) :
    """The actual function to fill PIDcalib histograms 
    - it books two histograms  (3D in this case)  ``accepted'' and ``rejected''
    - it fill them with 'accepted' and 'rejected' events (3D in this case)
    - finally one calculates the efficiency as
    >>> efficiency = 1/( 1 + rejected/accepted) 
    """
    ## create the object 
    def __init__ ( self             ,
                   accepted         , ## accepted sample 
                   rejected         , ## rejected sample 
                   xbins            , ## bins in 1st axis
                   ybins            , ## bins in 2nd axis
                   zbins            , ## bins in 3rd axis 
                   cuts     = None  , ## additional cuts (if any) 
                   ## "Accept"-function                              what  to project/draw                                 cuts&weight 
                   acc_fun  = lambda s,data : data.pproject ( s.ha , 'nTracks_Brunel : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.accepted , silent = True ) ,
                   ## "Reject"-function                              what  to project/draw                                 cuts&weight 
                   rej_fun  = lambda s,data : data.pproject ( s.hr , 'nTracks_Brunel : probe_Brunel_ETA : probe_Brunel_P/1000 ', '(%s)*probe_sWeight' % s.rejected , silent = True ) ) :
        
        ## initialize the base class
        PARTICLE_2D.__init__ ( self     ,
                               accepted ,
                               rejected , 
                               xbins    ,
                               ybins    ,
                               cuts     ,
                               acc_fun  ,
                               rej_fun  )

        ##  build 3d-histograms 
        from Ostap.PyRoUts import h3_axes
        
        self.ha  = h3_axes ( xbins , ybins , zbins , title = 'Accepted(%s)' % accepted ) 
        self.hr  = h3_axes ( xbins , ybins , zbins , title = 'Rejected(%s)' % rejected )

        
# =============================================================================
if '__main__' == __name__ :
    
    import Ostap.Line
    logger.info ( __file__  + '\n' + Ostap.Line.line  ) 
    logger.info ( 80*'*'   )
    logger.info ( __doc__  )
    logger.info ( 80*'*' )
    logger.info ( ' Author  : %s' %         __author__    ) 
    logger.info ( ' Version : %s' %         __version__   ) 
    logger.info ( ' Date    : %s' %         __date__      )
    logger.info ( ' Symbols : %s' %  list ( __all__     ) )
    logger.info ( 80*'*' ) 
    
# =============================================================================
# The END 
# =============================================================================
