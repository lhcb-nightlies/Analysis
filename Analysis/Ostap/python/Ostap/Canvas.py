#!/usr/bin/env python 
# -*- coding: utf-8 -*-
# =============================================================================
## @file Canvas.py
#  
#     .oooooo.                .                        
#    d8P'  `Y8b             .o8                        
#   888      888  .oooo.o .o888oo  .oooo.   oo.ooooo.  
#   888      888 d88(  "8   888   `P  )88b   888' `88b 
#   888      888 `"Y88b.    888    .oP"888   888   888 
#   `88b    d88' o.  )88b   888 . d8(  888   888   888 
#    `Y8bood8P'  8""888P'   "888" `Y888""8o  888bod8P' 
#                                            888       
#                                           o888o      
#                                                    
#  Simple helper module to get ROOT TCanvas
# 
#  This file is a part of 
#  <a href="http://cern.ch/lhcb-comp/Analysis/Bender/index.html">Bender project</a>
#  <b>``Python-based Interactive Environment for Smart and Friendly Physics Analysis''</b>
#
#  The package has been designed with the kind help from
#  Pere MATO and Andrey TSAREGORODTSEV. 
#  And it is based on the 
#  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
#  <b>``C++ ToolKit for Smart and Friendly Physics Analysis''</b>
#
#  By usage of this code one clearly states the disagreement 
#  with the smear campaign of Dr.O.Callot et al.: 
#  ``No Vanya's lines are allowed in LHCb/Gaudi software''
#
#  @date   2012-02-15
#  @author Vanya BELYAEV Ivan.Belyaevitep.ru
#
# =============================================================================
""" Simple helper module to get ROOT TCanvas
    
    This file is a part of BENDER project:

  ``Python-based Interactive Environment for Smart and Friendly Physics Analysis''

The project has been designed with the kind help from Pere MATO and Andrey TSAREGORODTSEV. 

And it is based on the LoKi project:
 
   ``C++ ToolKit for Smart and Friendly Physics Analysis''

By usage of this code one clearly states the disagreement with the smear campaign 
of Dr.O.Callot et al.:

   ``No Vanya's lines are allowed in LHCb/Gaudi software''
"""
# =============================================================================
__author__  = 'Vanya BELYAEV Ivan.Belyaev@itep.ru'
__date__    = "2014-10-19"
__version__ = '$Revision$'
__all__     = ( 'getCanvas'        ,
                'getCanvases'      ,
                'canvas_partition' , ## useful  NxM partition of canvas 
                'canvas_pull'      ) ## 1x2 partiotion, for pull/residual plots
# =============================================================================
import ROOT
# =============================================================================
# logging 
# =============================================================================
from Ostap.Logger import getLogger
if '__main__' ==  __name__ : logger = getLogger( 'Ostap.Canvas' )
else                       : logger = getLogger( __name__ )
# =============================================================================
_canvases = {} 
# =============================================================================
## get the canvas
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date 2014-10-19
def getCanvas ( name   = 'glCanvas' ,   ## canvas name 
                title  = 'Ostap'    ,   ## canvas title
                width  = 1000       ,   ## canvas width
                height = 800        ) : ## canvas height 
    """Get create canvas/create new canvas
    
    >>> cnv = getCanvas ( 'glnewCanvas' , width = 1200 , height = 1000 )
    """
    cnv   = _canvases.get ( name , None )
    if not cnv :
        ## create new canvas 
        ## cnv  = ROOT.TCanvas ( 'glCanvas', 'Ostap' , width , height )
        cnv  = ROOT.TCanvas ( name , 'Ostap' , width , height )
        ## adjust newly created canvas
        ## @see http://root.cern.ch/root/html/TCanvas.html#TCanvas:TCanvas@4
        if not ROOT.gROOT.IsBatch() :
            dw = width  - cnv.GetWw()
            dh = height - cnv.GetWh()
            cnv.SetWindowSize ( width + dw , height + dh )
            
        ## 
        _canvases [ name ] = cnv
        
    return cnv

# =============================================================================
## get all known canvases 
def getCanvases () :
    """ Get all known canvases """ 
    return _canvases.keys() 

def _remove_canvases_() :
    keys = _canvases.keys() 
    for k in keys : del _canvases[k]
        
import atexit
atexit.register ( _remove_canvases_ )

# =============================================================================
## perform partition of Canvas into
#  @code
#  canvas = ...
#  pads   = canvas.partition ( 3 , 2 )
#  for i in range(3) :
#    for j in range(2) :
#       histo_ij = ...
#       canvas.cd(0)
#       pad = pads[i,j]
#       pad.Draw()
#       pad.cd()
#       histo_ij.Draw()
#  @endcode
#  @see https://root.cern/doc/master/canvas2_8C.html
def canvas_partition ( canvas               , 
                       nx                   ,
                       ny                   ,
                       left_margin   = 0.14 ,
                       right_margin  = 0.05 ,   
                       bottom_margin = 0.16 ,
                       top_margin    = 0.05 ,
                       hSpacing      = 0.0  ,
                       vSpacing      = 0.0  ) :
    """Perform partition of Canvas into pads with no inter-margins

    canvas = ...
    nx = 3 , ny = 2 
    pads   = canvas.partition ( nx  , ny )
    for i in range(nx) :
    ... for j in range(ny) :
    ... ... histo_ij = ...
    ... ... canvas.cd(0)
    ... ... pad = pads[i,j]
    ... ... pad.Draw()
    ... ... pad.cd()
    ... ... histo_ij.Draw()
    
    @see https://root.cern/doc/master/canvas2_8C.html
    
    """

    if not isinstance ( nx , int ) or nx<= 0 :
        raise AttributeError('partition: invalid nx=%s' % nx )
    if not isinstance ( ny , int ) or ny<= 0 :
        raise AttributeError('partition: invalid ny=%s' % ny )

    ## get the window size
    wsx = abs ( canvas.GetWindowWidth  () ) 
    wsy = abs ( canvas.GetWindowHeight () ) 

    #
    ## if parametes given in the absolute units, convert them into relative coordinates
    #
    
    if   left_margin < 0 :   left_margin = abs (   left_margin ) / wsx
    if  right_margin < 0 :  right_margin = abs (  right_margin ) / wsx
    if bottom_margin < 0 : bottom_margin = abs ( bottom_margin ) / wsy
    if    top_margin < 0 :    top_margin = abs ( bottom_margin ) / wsy
    
    if hSpacing      < 0 : hSpacing = abs ( hSpacing ) / wsx
    if vSpacing      < 0 : vSpacing = abs ( vSpacing ) / wsy

    #
    ## check consistency 
    # 
    if 1 <=   left_margin :
        raise AttributeError('partition: invalid   left margin=%f' %   left_margin )
    if 1 <=  right_margin :
        raise AttributeError('partition: invalid  right margin=%f' %  right_margin )
    if 1 <= bottom_margin :
        raise AttributeError('partition: invalid bottom margin=%f' % bottom_margin )
    if 1 <=    top_margin :
        raise AttributeError('partition: invalid    top margin=%f' %    top_margin )
    
    if hasattr ( canvas , 'pads' ) :
        while canvas.pads :
            i,p = canvas.pads.popitem()
            logger.verbose ( 'delete pas %s' % p.GetName() )
            del p
    ## make new empty dictionary 
    canvas.pads = {} 
            
    vStep    = ( 1.0 - bottom_margin - top_margin   - (ny-1) * vSpacing ) / ny
    if 0 > vStep : raise AttributeError('partition: v-step=%f' % vStep  )
        
    hStep    = ( 1.0 - left_margin   - right_margin - (nx-1) * vSpacing ) / nx 
    if 0 > hStep : raise AttributeError('partition: h-step=%f' % hStep  )

    hposr, hposl, hmarr, hmarl, hfactor = 0.,0.,0.,0.,0.
    vposr, vposd, vmard, vmaru, vfactor = 0.,0.,0.,0.,0.
    
    for ix in range ( nx ) :
        
        if 0 == ix : 
            hposl   = 0
            hposr   = left_margin + hStep
            hfactor = hposr - hposl
            hmarl   = left_margin / hfactor
            hmarr   = 0.0 
        elif nx == ix + 1 :
            hposl   = hposr + hSpacing 
            hposr   = hposl + hStep + right_margin
            hfactor = hposr - hposl 
            hmarl   = 0.0
            hmarr   = right_margin / hfactor 
        else : 
            hposl   = hposr + hSpacing
            hposr   = hposl + hStep
            hfactor = hposr - hposl
            hmarl   = 0.0
            hmarr   = 0.0

        for iy in range(ny) :
            if 0 == iy : 
                vposd   = 0.0
                vposu   = bottom_margin + vStep
                vfactor = vposu - vposd
                vmard   = bottom_margin / vfactor
                vmaru   = 0.0 
            elif ny == iy + 1 : 
                vposd   = vposu + vSpacing
                vposu   = vposd + vStep + top_margin
                vfactor = vposu - vposd;
                vmard   = 0.0
                vmaru   = top_margin    / vfactor 
            else :
                vposd   = vposu + vSpacing
                vposu   = vposd + vStep
                vfactor = vposu - vposd
                vmard   = 0.0
                vmaru   = 0.0

            canvas.cd(0)
            pname = 'pad_%s_%d_%d' % ( canvas.GetName() , ix , iy )
            pad   = ROOT.gROOT.FindObject ( pname )
            if pad : del pad
            pad   = ROOT.TPad ( pname , '' ,  hposl , vposd  , hposr , vposu )

            logger.verbose ( ' Create pad[%d,%d]=(%f,%f,%f,%f),[%f,%f,%f,%f] %s ' % (
                ix    , iy    ,
                hposl , vposd , hposr, vposu , 
                hmarl , hmarr , vmard , vmaru , pad.GetName() ) ) 
                             
            pad.SetLeftMargin      ( hmarl )
            pad.SetRightMargin     ( hmarr )
            pad.SetBottomMargin    ( vmard )
            pad.SetTopMargin       ( vmaru )
            
            pad.SetFrameBorderMode ( 0 )
            pad.SetBorderMode      ( 0 )
            pad.SetBorderSize      ( 0 )

            ROOT.SetOwnership ( pad , True )
            
            if not hasattr ( canvas , 'pads' ) : canvas.pads = {}
            canvas.pads[ (ix,iy) ] = pad
            
    return canvas.pads 


ROOT.TCanvas.partition = canvas_partition

# ==============================================================================
##  Perform partition of Canvas into 1x2 non-equal pads with no inter-margins
#  @code
#  canvas    = ...
#  pad_u, pud_b= canvas.pull_partition ( 0.20 )    
#  canvas.cd(0)
#  pad_u.Draw()
#  pad_u.cd() 
#  object1.Draw()    
#  canvas.cd(0)
#  pad_b.Draw()
#  pad_b.cd() 
#  object2.Draw()
#  @endcode 
def canvas_pull ( canvas               ,
                  ratio         = 0.80 ,
                  left_margin   = 0.14 ,
                  right_margin  = 0.05 ,   
                  bottom_margin = 0.16 ,
                  top_margin    = 0.05 ,
                  hSpacing      = 0.0  ,
                  vSpacing      = 0.0  ) :
    """Perform partition of Canvas into 1x2 non-equal pads with no inter-margins
    
    >>> canvas    = ...
    >>> pad_u, pud_b= canvas.pull_partition ( 0.20 )
    
    >>> canvas.cd(0)
    >>> pad_u.Draw()
    >>> pad_u.cd() 
    >>> object1.Draw()
    
    >>> canvas.cd(0)
    >>> pad_b.Draw()
    >>> pad_b.cd() 
    >>> object2.Draw()
    """

    ## get the window size
    wsx = abs ( canvas.GetWindowWidth  () ) 
    wsy = abs ( canvas.GetWindowHeight () ) 

    #
    ## if parametes given in the absolute units, convert them into relative coordinates
    #
    
    if   left_margin < 0 :   left_margin = abs (   left_margin ) / wsx
    if  right_margin < 0 :  right_margin = abs (  right_margin ) / wsx
    if bottom_margin < 0 : bottom_margin = abs ( bottom_margin ) / wsy
    if    top_margin < 0 :    top_margin = abs ( bottom_margin ) / wsy
    
    if hSpacing      < 0 : hSpacing = abs ( hSpacing ) / wsx
    if vSpacing      < 0 : vSpacing = abs ( vSpacing ) / wsy
    
    hposr, hposl, hmarr, hmarl, hfactor = 0.,0.,0.,0.,0.
    vposr, vposd, vmard, vmaru, vfactor = 0.,0.,0.,0.,0.

    nx = 1
    ny = 2
    
    vStep    = ( 1.0 - bottom_margin - top_margin   - (ny-1) * vSpacing ) / ny
    if 0 > vStep : raise AttributeError('partition: v-step=%f' % vStep  )
    
    hStep    = ( 1.0 - left_margin   - right_margin - (nx-1) * vSpacing ) / nx 
    if 0 > hStep : raise AttributeError('partition: h-step=%f' % hStep  )
    
    hposl   = 0
    hposr   = left_margin + hStep
    hfactor = hposr - hposl
    hmarl   = left_margin / hfactor
    hmarr   = 0.0
    
    if hasattr ( canvas ,  'pads' ) :
        del canvas.pads


    vStep0 = 2 * vStep * 1     / ( 1 + ratio )
    vStep1 = 2 * vStep * ratio / ( 1 + ratio )
    
    ix = 0 
    for iy in range(2) :
        
        if 0 == iy : 
            vposd   = 0.0
            vposu   = bottom_margin + vStep0
            vfactor = vposu - vposd 
            vmard   = bottom_margin / vfactor
            vmaru   = 0.0 
        else : 
            vposd   = vposu + vSpacing
            vposu   = vposd + vStep1 + top_margin
            vfactor = vposu - vposd
            vmard   = 0.0
            vmaru   = top_margin    / vfactor
            
        canvas.cd(0)
        pname = 'pad_%s_%d_%d' % ( canvas.GetName() , ix , iy )
        pad   = ROOT.gROOT.FindObject ( pname )
        if pad : del pad
        pad   = ROOT.TPad ( pname , '' ,  hposl , vposd  , hposr , vposu )
        
        logger.verbose ( ' Create pad[%d,%d]=(%f,%f,%f,%f),[%f,%f,%f,%f] %s ' % (
            ix    , iy    ,
            hposl , vposd , hposr, vposu , 
            hmarl , hmarr , vmard , vmaru , pad.GetName() ) ) 
        
        pad.SetLeftMargin      ( hmarl )
        pad.SetRightMargin     ( hmarr )
        pad.SetBottomMargin    ( vmard )
        pad.SetTopMargin       ( vmaru )
        
        pad.SetFrameBorderMode ( 0 )
        pad.SetBorderMode      ( 0 )
        pad.SetBorderSize      ( 0 )
        
        ROOT.SetOwnership ( pad , True )
        
        if not hasattr ( canvas , 'pads' ) : canvas.pads=[]
        canvas.pads.append ( pad ) 
        
    canvas.pads =  tuple(reversed(canvas.pads))
    return canvas.pads 


ROOT.TCanvas.pull_partition = canvas_pull

if not hasattr ( ROOT.TObject , 'draw' ) :
    
    ## save old method
    ROOT.TObject._old_draw_ = ROOT.TObject.Draw
    from Ostap.Utils import  rootWarning
    ##  new draw method 
    def _to_draw_ ( obj , *args , **kwargs ) :
        """ Draw ROOT object
        >>> obj
        >>> obj.Draw()  ##
        >>> obj.draw()  ## ditto         
        """
        with rootWarning() :
            return obj.Draw( *args , **kwargs )
        
    ROOT.TObject.draw = _to_draw_
        
# =============================================================================
if '__main__' == __name__ :
    
    from Ostap.Line import line 
    logger.info ( __file__ + '\n' + line  )
    logger.info ( 80*'*' )
    logger.info ( __doc__  )
    logger.info ( 80*'*' )
    logger.info ( ' Author  : %s' %         __author__    ) 
    logger.info ( ' Version : %s' %         __version__   ) 
    logger.info ( ' Date    : %s' %         __date__      )
    logger.info ( ' Symbols : %s' %  list ( __all__     ) )
    logger.info ( 80*'*' ) 

# =============================================================================
# The END 
# =============================================================================
